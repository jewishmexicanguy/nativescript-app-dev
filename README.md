# NativeScript-app-dev

Initial foray into using open source tools and framework for mobile application development

## initial remarks
Having a tutorial where you establish is fast workflow from playground in a browser tool to deploying to your phone is really cool. Scary but really cool.

Completed tutorial

## install nativescript cli
npm install nativescript -g

## create new app
tns create MyApp
cd MyApp

## Preview your app
tns preview